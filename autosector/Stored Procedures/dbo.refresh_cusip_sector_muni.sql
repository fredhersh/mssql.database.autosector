﻿
CREATE PROCEDURE [dbo].[refresh_cusip_sector_muni] AS

/**********************************************************************************************************************************************************************
**
**	FHersh		02/26/2020	Created.
**	FHersh		06/10/2020	Adjusted muni_typ definition per Les F.
**	FHersh		08/18/2020	Added is_be_cusip field.
**	FHersh		11/04/2020	For each cusip get the muni.bondinfo record with the latest
**							file_dt.
**
**********************************************************************************************************************************************************************/

SET NOCOUNT ON

DECLARE	@sectoring_dt	DATE

SET	@sectoring_dt = GETDATE()

/*
** Create cusip_with_sector if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.SCHEMA_ID = s.schema_id WHERE t.name = 'cusip_with_sector' AND t.type='U' AND s.NAME = 'dbo')
BEGIN

	CREATE TABLE [dbo].[cusip_with_sector](
		[cusip] [varchar](20) NOT NULL,
		[is_be_cusip] [varchar](3) NOT NULL,
		[muni_typ] [varchar](20) NULL,
		[maturity_no_i] [int] NULL,
		[maturity_no_f] [decimal](6, 3) NULL,
		[call_no_i] [int] NULL,
		[call_no_f] [numeric](6, 3) NULL,
		[sector_num] [varchar](5) NULL,
		[bbg_sector_cd] [varchar](10) NULL,
		[sectoring_dt] [date] NULL,
	 CONSTRAINT [pk_cusip_with_sector] PRIMARY KEY CLUSTERED 
	(
		[cusip] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END

/*
** Get all muni cusips which either have not yet matured or have only matured in the last year
*/

SELECT DISTINCT	cusip = cusip_c
,				sectoring_dt = @sectoring_dt
INTO			#cusips
FROM			mergent.muni.bondinfo bi
WHERE			maturity_date_d >= DATEADD(YEAR, -1, @sectoring_dt)

CREATE UNIQUE CLUSTERED INDEX xpk ON #cusips(cusip)

/*
** Get the relavant fields that are needed to sector the cusip
*/

SELECT	cs.cusip
,		is_be_cusip =	CASE
						WHEN co.cusip IS NOT NULL THEN 'Yes'
						ELSE 'No'
						END
,		sectoring_dt = CAST(cs.sectoring_dt AS DATE)
,		bond_typ =	CASE
					WHEN bi.bank_qualified_i = 'Y' THEN 'BQ'
					WHEN bi.tax_code_c = 'TAX' THEN 'Taxable'
					WHEN ((bi.bank_qualified_i <> 'Y' AND bi.tax_code_c <> 'TAX' AND DATEPART(YEAR, bi.dated_date_d) IN (2009, 2010) AND CHARINDEX('refund', c.code_desc_c) = 0) OR co.cusip IS NOT NULL) THEN 'BE'
					ELSE 'GMM'
					END
,		muni_dated_dt = bi.dated_date_d
,		coupon_f = ISNULL(bi.coupon_f, 0.0)
,		bi.maturity_date_d
,		bi.maturity_amount_f
,		bi.next_call_date_d
,		bi.next_call_price_f
,		bi.update_date_d
,		px_bid = maturity_amount_f
,		price = offering_price_f
,		maturity_no_f = CAST(DATEDIFF(DAY, cs.sectoring_dt, bi.maturity_date_d) / 365.0 AS DECIMAL(6,3))
,		call_no_f = CASE
					WHEN bi.next_call_date_d IS NULL THEN 0.0
					ELSE CAST(DATEDIFF(DAY, cs.sectoring_dt, bi.next_call_date_d) / 365.0 AS DECIMAL(6,3))
					END
,		muni_purpose = c.code_desc_c
INTO	#cusips_with_general_info
FROM	#cusips cs	INNER JOIN mergent.muni.bondinfo bi ON (cs.cusip = bi.cusip_c)
					LEFT JOIN pvt.cusip_override co ON (cs.cusip = co.cusip)
					LEFT JOIN mergent.muni.codes c ON (bi.capital_purpose_c = c.code_c AND c.code_col_name_c = 'CAPITAL_PURPOSE_C')
WHERE	bi.maturity_date_d >= cs.sectoring_dt
AND		(bi.next_call_date_d >= cs.sectoring_dt OR bi.next_call_date_d IS NULL)
AND		bi.file_dt = (SELECT MAX(bil.file_dt) FROM mergent.muni.bondinfo bil WHERE bi.cusip_c = bil.cusip_c)

/*
** Get the lookup fields that are needed to sector the cusip
*/

SELECT	*
,		maturity_no_i = CAST(ROUND(maturity_no_f, 0) AS INT)
,		call_no_i = CAST(ROUND(call_no_f, 0) AS INT)
--,		muni_typ =	CASE
--					WHEN bond_typ = 'BQ' AND coupon_f >= 3.4 THEN 'BQ Prem'
--					WHEN bond_typ = 'BQ' AND coupon_f < 3.4 AND coupon_f > 0.0 THEN 'BQ Par'
--					WHEN bond_typ = 'BQ' AND coupon_f = 0.0 AND px_bid >= 104 THEN 'BQ Zero Prem'
--					WHEN bond_typ = 'BQ' AND coupon_f = 0.0 AND px_bid < 104 THEN 'BQ Zero Par'
--					WHEN bond_typ = 'GMM' AND coupon_f >= 4.5 THEN 'GMM High Prem'
--					WHEN bond_typ = 'GMM' AND coupon_f < 4.5 AND coupon_f >= 3.8 THEN 'GMM Mod Prem'
--					WHEN bond_typ = 'GMM' AND coupon_f < 3.8 AND coupon_f > 0.0 THEN 'GMM Par Prem'
--					WHEN bond_typ = 'GMM' AND coupon_f = 0.0 AND price >= 104.0 THEN 'GMM Zero Prem'
--					WHEN bond_typ = 'GMM' AND coupon_f = 0.0 AND px_bid < 104.0 THEN 'GMM Zero Par'
--					WHEN bond_typ = 'BE' AND coupon_f >= 4.5 THEN 'BE High Prem'
--					WHEN bond_typ = 'BE' AND coupon_f < 4.5 AND coupon_f >= 0.0 THEN 'BE Mod Prem'
--					WHEN bond_typ = 'BE' AND coupon_f = 0.0 AND px_bid >= 104.0 THEN 'BE Zero Prem'
--					WHEN bond_typ = 'Taxable' AND coupon_f >= 4.2 THEN 'Taxable High Prem'
--					WHEN bond_typ = 'Taxable' AND coupon_f < 4.2 THEN 'Taxable Low Prem'
--					END
,		muni_typ =	CASE

					WHEN bond_typ = 'BE' AND coupon_f < 1.0  THEN 'BE Zero Prem'
					WHEN bond_typ = 'BE' AND coupon_f >= 1.0 AND coupon_f <= 4.5 THEN 'BE Mod Prem'
					WHEN bond_typ = 'BE' AND coupon_f > 4.5 THEN 'BE High Prem'

					WHEN bond_typ = 'BQ' AND coupon_f < 1.0 THEN 'BQ Zero Prem'
					WHEN bond_typ = 'BQ' AND coupon_f >= 1.0 AND coupon_f <= 3.5 THEN 'BQ Par'
					WHEN bond_typ = 'BQ' AND coupon_f > 3.5 THEN 'BQ Prem'

					WHEN bond_typ = 'GMM' AND coupon_f < 1.0 THEN 'GMM Zero Prem'
					WHEN bond_typ = 'GMM' AND coupon_f >= 1.0 AND coupon_f <= 3.5 THEN 'GMM Par Prem'
					WHEN bond_typ = 'GMM' AND coupon_f > 3.5 AND coupon_f <= 4.5 THEN 'GMM Mod Prem'
					WHEN bond_typ = 'GMM' AND coupon_f > 4.5 THEN 'GMM High Prem'

					WHEN bond_typ = 'Taxable' AND coupon_f < 4.2 THEN 'Taxable Low Prem'
					WHEN bond_typ = 'Taxable' AND coupon_f >= 4.2 THEN 'Taxable High Prem'

					END
INTO	#cusips_with_lookup_info
FROM	#cusips_with_general_info


/*
** Refesh the cusip with sector persisted table
*/

TRUNCATE TABLE cusip_with_sector

INSERT	cusip_with_sector
(cusip
,is_be_cusip
,muni_typ
,maturity_no_i
,maturity_no_f
,call_no_i
,call_no_f
,sector_num
,lpfm_sector_nm
,bbg_sector_cd
,sectoring_dt
)
SELECT	p.cusip
,		p.is_be_cusip
,		p.muni_typ
,		p.maturity_no_i
,		p.maturity_no_f
,		p.call_no_i
,		p.call_no_f
,		slm.[Sector Number]
,		s.lpfm_sector_nm
,		s.bbg_sector_cd
,		p.sectoring_dt
FROM	#cusips_with_lookup_info p	LEFT JOIN sector_lookup_muni slm on (p.muni_typ = slm.[Muni Type] and p.maturity_no_i = slm.[Maturity Number] and p.call_no_i = slm.[Call Number] AND slm.[Sector Number] <> '-')
									LEFT JOIN prd.sector s ON (cast(slm.[Sector Number] AS INT) = s.sector_num)


/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_cusip_with_sector' AND object_id = OBJECT_ID('dbo.cusip_with_sector'))
BEGIN
	ALTER TABLE dbo.cusip_with_sector ADD  CONSTRAINT pk_cusip_with_sector PRIMARY KEY CLUSTERED (cusip ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END


/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#cusips')) IS NOT NULL
	DROP TABLE #cusips

IF (OBJECT_ID('tempdb..#cusips_with_general_info')) IS NOT NULL
	DROP TABLE #cusips_with_general_info
	
IF (OBJECT_ID('tempdb..#cusips_with_lookup_info')) IS NOT NULL
	DROP TABLE #cusips_with_lookup_info

