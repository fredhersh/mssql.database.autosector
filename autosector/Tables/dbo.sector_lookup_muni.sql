﻿CREATE TABLE [dbo].[sector_lookup_muni] (
    [Muni Type]       VARCHAR (50) NOT NULL,
    [Maturity Number] INT          NOT NULL,
    [Call Number]     INT          NOT NULL,
    [Sector Number]   VARCHAR (5)  NOT NULL
);

