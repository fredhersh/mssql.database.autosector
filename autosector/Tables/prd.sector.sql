﻿CREATE TABLE [prd].[sector] (
    [sector_id]                  INT           NOT NULL,
    [sector_num]                 INT           NULL,
    [created_nm]                 VARCHAR (64)  NOT NULL,
    [created_dt]                 SMALLDATETIME NOT NULL,
    [modified_nm]                VARCHAR (64)  NULL,
    [modified_dt]                SMALLDATETIME NULL,
    [removed_nm]                 VARCHAR (64)  NULL,
    [removed_dt]                 SMALLDATETIME NULL,
    [code]                       VARCHAR (64)  NOT NULL,
    [long_nm]                    VARCHAR (128) NOT NULL,
    [description]                VARCHAR (256) NULL,
    [lpfm_sector_nm]             VARCHAR (256) NULL,
    [sector_rep_cd]              VARCHAR (32)  NULL,
    [asset_bal_sheet_cd]         VARCHAR (32)  NULL,
    [asset_class_level1_cd]      VARCHAR (32)  NULL,
    [asset_class_level2_cd]      VARCHAR (32)  NULL,
    [asset_class_level3_cd]      VARCHAR (32)  NULL,
    [tax_status_cd]              VARCHAR (32)  NULL,
    [issuance_cd]                VARCHAR (32)  NULL,
    [rating_cd]                  VARCHAR (32)  NULL,
    [industry_cd]                VARCHAR (32)  NULL,
    [collat_desc]                VARCHAR (64)  NULL,
    [struct_principal_cd]        VARCHAR (32)  NULL,
    [des_maturity_years_num]     FLOAT (53)    NULL,
    [des_first_option_years_num] FLOAT (53)    NULL,
    [des_maturity_bucket_cd]     VARCHAR (32)  NULL,
    [des_cpn_type_cd]            VARCHAR (32)  NULL,
    [struct_price_cd]            VARCHAR (32)  NULL,
    [struct_cmo_type_cd]         VARCHAR (32)  NULL,
    [cpn_value]                  FLOAT (53)    NULL,
    [call_freq_years_num]        FLOAT (53)    NULL,
    [short_nm]                   VARCHAR (64)  NULL,
    [sector_client_type_cd]      VARCHAR (32)  NULL,
    [market_sector_cd]           VARCHAR (8)   NULL,
    [asset_class_type_cd]        VARCHAR (16)  NULL,
    [lpf_sector_flg]             BIT           NOT NULL,
    [lpf_bank_flg]               BIT           NOT NULL,
    [lpf_cu_flg]                 BIT           NOT NULL,
    [bbg_sector_cd]              VARCHAR (8)   NULL,
    [sector_dreg_cd]             VARCHAR (16)  NULL,
    CONSTRAINT [pk_sector] PRIMARY KEY NONCLUSTERED ([sector_id] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [idx_sector_sector_num]
    ON [prd].[sector]([sector_num] ASC);

