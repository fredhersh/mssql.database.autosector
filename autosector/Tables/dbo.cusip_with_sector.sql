﻿CREATE TABLE [dbo].[cusip_with_sector] (
    [cusip]          VARCHAR (20)   NOT NULL,
    [is_be_cusip]    VARCHAR (3)    NOT NULL,
    [muni_typ]       VARCHAR (20)   NULL,
    [maturity_no_i]  INT            NULL,
    [maturity_no_f]  DECIMAL (6, 3) NULL,
    [call_no_i]      INT            NULL,
    [call_no_f]      NUMERIC (6, 3) NULL,
    [sector_num]     VARCHAR (5)    NULL,
    [lpfm_sector_nm] VARCHAR (256)  NULL,
    [bbg_sector_cd]  VARCHAR (10)   NULL,
    [sectoring_dt]   DATE           NULL,
    CONSTRAINT [pk_cusip_with_sector] PRIMARY KEY CLUSTERED ([cusip] ASC)
);





