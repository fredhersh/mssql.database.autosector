﻿CREATE TABLE [pvt].[cusip_override] (
    [cusip] VARCHAR (20) NOT NULL,
    CONSTRAINT [pk_cusip_override] PRIMARY KEY CLUSTERED ([cusip] ASC)
);

